#include <iostream>
#include <iomanip>
using namespace std;

class geometricobject { //baseclass
	public:
		virtual void geo( ) {
			cout << "Just a geometric object" << endl;
		}
	
	protected:
		float base= 0;
		float length= 0;
		float height= 0;
		float width= 0;
		float radius= 0;
		float pi= 3.14159f;
				
};

class rectangle : public geometricobject { //derived class
	public:
		virtual void geo( ) {
//			int length= 0,width= 0;
			cout <<"I'm a rectangle, enter my values:"<<endl;
			
			cout<<">>length: ";
			cin>>length;
			cout<<">>Width: ";
			cin>>width;
			
			cout<<"\tMy area is: "<<fixed<<setprecision(2)<<length*width<<endl;
		}
};

class circle : public geometricobject { // derived class
	public:
		virtual void geo( ) {
//			int radius= 0, pi= 3.14159;
			cout << "I'm a circle, enter my values:\n>>Radius: ";
			cin>>radius;
			
			cout<<"\tMy area is: "<<fixed<<setprecision(2)<<pi* (radius*radius)<<endl;
		}
};

class trapezoid : public geometricobject {
	public:
		virtual void geo() {
			int a= 0,b= 0;//,h= 0;
			cout<<"I'm a trapezoid, enter my values:"<<endl;			
			
			cout<<">>Base a: ";
			cin>>a;
			cout<<">>Base b: ";
			cin>>b;
			cout<<">>Height: ";
			cin>>height;

			cout<<"\tMy area is: "<<fixed<<setprecision(2)<<((float)(a+b)/2) *height<<endl;
		}
};

class triangle : public geometricobject {
	public:
		virtual void geo() {
			cout<<"I'm a triangle, enter my values: "<<endl;
			
			cout<<">>Base: ";
			cin>>base;
			cout<<">>Height: ";
			cin>>height;
			
			cout<<"\tMy area is: "<<fixed<<setprecision<<(float)(base*height) /2<<endl;
		}
};

int main( ) {
	geometricobject *ptr; //pointer to base class
	char which=' ';

	while(which!= 'q') {
		do {
			cout << "Enter:\n\t"
			     <<"1 for geometricobject\n\t"
			     <<"2 for rectangle\n\t"
			     <<"3 for circle\n\t"
			     <<"4 for trapezoid\n\t"
			     <<"5 for triangle\n\t"
			     <<"q to quit\n"<<endl;
			cin >> which;

			if(which=='q') break; //break loop
		} while (which < '1' || which > '5');  //loop until valid menu selection

		if(which=='q') break; //exit program

		switch (which) { //set pointer depending on user choice
			case '1':
				ptr= new geometricobject;
				break;
			case '2':
				ptr= new rectangle;
				break;
			case '3':
				ptr= new circle;
				break;
			case '4':
				ptr= new trapezoid;
				break;
			case '5':
				ptr= new triangle;
				break;
		}

		ptr -> geo( ); //run-time binding
		delete ptr; //don�t leave junk hanging around
	}
	return 0;
}
